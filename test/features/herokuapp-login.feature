@XRAYP-1
Feature: Performing Yahoo Search part two

    # As a user on the Yahoo search page
    # I want to search for Selenium-Webdriver
    # Because I want to learn more about it

    Background:

        Given I am on the search page
        @XRAYP-4
    Scenario: Performing a search operation two on heroku
        When I enter "Selenium Webdriver" into the search box
        And  I click the search button
        Then I should see a list of search results
        @XRAYP-5
    Scenario Outline: Performing a search operation with passing test data as data table on heroku
        When I enter "<searchItem>" into the search box
        And  I click the search button
        Then I should see a list of search results

        Examples:
            | searchItem           |
            | Incredible Hulk |
